﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Playbot
{
    [Serializable]
    public class BotControlProbability
    {
        public string Name;
        public bool IsActive = true;
        public string Comment;
        public string Path;
        public float Probability;
        public float DefaultProbability;

        public string ActionMessage;

        [Tooltip("Delay after action, MSec")]
        public int DelayMsec;
        public UnityEvent Event;

        public bool IsEqual(string name, string fullPath)
        {
            return string.IsNullOrEmpty(Path) || string.IsNullOrEmpty(fullPath)
                       ? string.Equals(Name, name)
                       : fullPath.EndsWith(Path);
        }

        public void SetProbability(float probability) => Probability = probability;
    }
}
