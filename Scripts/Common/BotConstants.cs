﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Playbot
{
    public class BotConstants
    {
        public static readonly Guid HelloMessage_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000000");
        public static readonly Guid HelloMessage_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000000");

        public static readonly Guid RequestScreenshot_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000001");
        public static readonly Guid SendScreenshot_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000001");

//        public static readonly Guid RequestControls_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000002");
//        public static readonly Guid SendControls_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000002");

        public static readonly Guid RequestControlsList_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000003");
        public static readonly Guid SendControlsList_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000003");

        // request data from in-game spy
        // it can be object with access to injection binder, models, etc
        public static readonly Guid RequestSpyData_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000004");
        public static readonly Guid SendSpyData_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000004");

        public static readonly Guid RequestSendMessage_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000005");
        public static readonly Guid ResponseSendMessage_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000005");

        // send simulate click
        public static readonly Guid SimulateClick_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000010");
        public static readonly Guid SimulateClick_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000010");

        // send/receive message
        public static readonly Guid SendMessageToObject_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000011");
        public static readonly Guid ReceivedMessageFromObject_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000011");

        // send/receive message
        public static readonly Guid SendReportToSlack_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000012");
        public static readonly Guid SendReportToSlack_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000012");

        // for simulate request/responses (game request recorded data and bot provide it to game)
        public static readonly Guid ResponseEndpoint_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000031");
        public static readonly Guid RequestEndpoint_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000031");

        // messages to game
        public static readonly Guid SendSocketData_BotToGame = new Guid("5aed62fd-007d-46e9-0000-000000000032");
        public static readonly Guid SocketData_GameToBot = new Guid("5aed62fd-007d-46e9-1111-000000000032");
    }

    [Serializable]
    public class ClickData
    {
        public Vector2 Position;
        public int TimeMSec = 200;
    }

    [Serializable]
    public class BotTransformInfo
    {
        [SerializeField]
        public string Name;

        [SerializeField]
        public Vector2 Position;

        [SerializeField]
        public List<BotTransformInfo> Children;
    }
}
