﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Playbot
{
    [Serializable]
    public class BotDataObjectWithEvent : BaseBotDataObject
    {
        [Serializable]
        public class DataObjectWithEventDataChange : UnityEvent<BaseBotDataObject> { }

        [SerializeField]
        public DataObjectWithEventDataChange OnDataChange;

        public override void DataChanged(BaseBotDataObject obj)
        {
            OnDataChange?.Invoke(obj);
        }

        public void UpdateUnityEventTarget(UnityEngine.Object from, UnityEngine.Object to)
        {
            for (var i = 0; i < OnDataChange.GetPersistentEventCount(); i++)
            {
                var objFrom = OnDataChange.GetPersistentTarget(i);
                if (objFrom.GetType() == from.GetType())
                {
                    var methodName = OnDataChange.GetPersistentMethodName(i);
                    var action = (UnityAction<BaseBotDataObject>)Delegate.CreateDelegate(typeof(UnityAction<BaseBotDataObject>), to, methodName);
                    OnDataChange.AddListener(action);
                    OnDataChange.SetPersistentListenerState(i, UnityEventCallState.Off);
                }
            }
        }
    }
}
