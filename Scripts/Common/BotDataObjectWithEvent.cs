﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playbot
{
    [Serializable]
    public class BotDataObjectWithEventList
    {
        public List<BotDataObjectWithEvent> List = new List<BotDataObjectWithEvent>();

        public void UpdateUnityEventTarget(UnityEngine.Object from, UnityEngine.Object to)
        {
            foreach (var entry in List)
            {
                entry.UpdateUnityEventTarget(from, to);
            }
        }

        public int GetEventsCount()
        {
            return List.Sum(entry => entry.OnDataChange.GetPersistentEventCount());
        }

        public long GetValue(string name)
        {
            var element = List.FirstOrDefault(x => string.Equals(x.Name, name));
            if (element == null)
            {
                throw new Exception($"Can't find value for {name}");
            }
            // check type
            return (long)element.Object;
        }
    }
}
