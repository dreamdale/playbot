﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbot
{
    [Serializable]
    public class BotControlProbabilities
    {
        public List<BotControlProbability> List = new List<BotControlProbability>();

        [Preserve]
        public void SetZeroProbability(string fieldPath)
        {
            SetProbability(fieldPath, (field) => { field.SetProbability(0f); });
        }

        [Preserve]
        public void SetDefaultProbability(string fieldPath)
        {
            SetProbability(fieldPath, (field) => { field.SetProbability(field.DefaultProbability); });
        }

        [Preserve]
        public void SetProbability(string fieldPath, Action<BotControlProbability> action)
        {
            var fields = List.FindAll(x => string.Equals(fieldPath, x.Path));
            if (fields.Count == 0)
            {
                Debug.LogError($"Field {fieldPath} not found!");
                return;
            }

            foreach (var field in fields)
            {
                action?.Invoke(field);
            }
        }
    }
}
