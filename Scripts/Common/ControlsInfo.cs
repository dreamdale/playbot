﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Playbot
{
    [Serializable]
    public class ControlsInfo
    {
        public string Name;
        public string FullPath;
        public int Id;
        [SerializeField]
        public Rect Rect;
    }

    [Serializable]
    public class ControlsInfoList
    {
        public List<ControlsInfo> List = new List<ControlsInfo>();
    }
}
