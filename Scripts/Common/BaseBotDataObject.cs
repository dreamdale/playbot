﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Playbot
{
    [Serializable]
    public class BaseBotDataObject : ISerializationCallbackReceiver
    {
        public string Name;
        public object Object;
        public string ObjectData;
        public string ObjectType;

        public void OnBeforeSerialize()
        {
            if (Object != null)
            {
                ObjectData = Object.ToString();
                ObjectType = Object.GetType().ToString();
            }
        }

        public void OnAfterDeserialize()
        {
            if (string.IsNullOrEmpty(ObjectData))
            {
                return;
            }

            if (string.IsNullOrEmpty(ObjectType))
            {
                return;
            }

            if (Object == null || !string.Equals(Object.ToString(), ObjectData))
            {
                var type = Type.GetType(ObjectType);
                if (type == null) return; // obj type not found - write error

                Object = ObjectData;
                Object = Convert.ChangeType(Object, type);
                DataChanged(this);
            }
        }

        public virtual void DataChanged(BaseBotDataObject obj) { }
    }

    [Serializable]
    public class BaseBotDataObjectList
    {
        public List<BaseBotDataObject> List = new List<BaseBotDataObject>();
    }
}
