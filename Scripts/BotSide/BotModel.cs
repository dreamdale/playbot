﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    [CreateAssetMenu(fileName = nameof(BotModel), menuName = nameof(Playbot) + "/Create " + nameof(BotModel))]
    public class BotModel : ScriptableObject
    {
        [Header("List of data fields from game")]
        [Tooltip("Bot update game data from in-game Bot Spy.")]
        [SerializeField]
        private BotDataObjectWithEventList _botDataObjectWithEvents = default;

        [Header("List of probabilities to select any button")]
        [Tooltip("Probability of selection any button")]
        [SerializeField]
        private BotControlProbabilities _botControlProbabilities = default;

        private const int DELAY_AFTER_CONTROLS_RECEIVED = 1000;
        private const int DELAY_ON_NO_CONTROL = 1000;
        private const int DEFAULT_DELAY_AFTER_CLICK = 2000;

        public List<BotControlProbability> Probabilities => _botControlProbabilities.List;
        public ControlsInfoList ControlsInfoList => _controlsInfoList;
        public BotDataObjectWithEventList BotDataObjectWithEventList => _botDataObjectWithEvents;
        public BotControlProbabilities BotControlProbabilities => _botControlProbabilities;
        public float TotalProbability => GetActualProbabilities.Sum(x => x.Probability);
        public bool IsBotPlay => _cancellationTokenSource != null;
        public event Action<object> Log;

        private IBotConnectionManager _botConnectionManager;
        private CancellationTokenSource _cancellationTokenSource;
        private ControlsInfoList _controlsInfoList;
        private bool _waitForListUpdate;
        private bool _waitForGameDataUpdate;
        private readonly List<Tuple<Guid, UnityAction<MessageEventArgs>>> _availableActions = new List<Tuple<Guid, UnityAction<MessageEventArgs>>>();
        private IEnumerable<BotControlProbability> GetActualProbabilities =>
            Probabilities.Where(
                probability => ControlsInfoList.List.Any(
                    controlsInfo => probability.IsEqual(controlsInfo.Name, controlsInfo.FullPath) && probability.IsActive));

        public static BotModel CreateBotModel()
        {
            var botModelPrefab = Resources.Load<BotModel>($"{nameof(Playbot)}/{nameof(BotModel)}");
            var botModel = Instantiate(botModelPrefab);
            botModel.BotDataObjectWithEventList.UpdateUnityEventTarget(botModelPrefab, botModel);
            return botModel;
        }

        public void SetConnectionManager(IBotConnectionManager botConnectionManager)
        {
            _botConnectionManager = botConnectionManager;
        }

        public void FillList()
        {
            _availableActions.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(BotConstants.HelloMessage_GameToBot, OnHelloMessageReceived));
            _availableActions.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(BotConstants.SendScreenshot_GameToBot, OnScreenshotReceived));
            _availableActions.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(BotConstants.SendControlsList_GameToBot, OnControlsListReceived));
            _availableActions.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(BotConstants.SendSpyData_GameToBot, OnSpyDataReceived));
        }

        public void Register()
        {
            foreach (var (guid, unityAction) in _availableActions)
            {
                _botConnectionManager.RegisterEditorMessage(guid, unityAction);
                _botConnectionManager.Log(guid + " Register " + unityAction.Method.Name);
            }
        }

#region Messages Callbacks
        private void OnHelloMessageReceived(MessageEventArgs args)
        {
            var text = Encoding.ASCII.GetString(args.data);
            _botConnectionManager.Log("Message from player: " + text);
            Log(text);
        }

        private void OnScreenshotReceived(MessageEventArgs args)
        {
            _botConnectionManager.Log("OnScreenshot from player: ");

            var tex = new Texture2D(2, 2);
            tex.LoadImage(args.data);
            Log($"Get screenshot {tex.width} x {tex.height}");
            Log(tex);
        }

        private void OnControlsListReceived(MessageEventArgs args)
        {
            var text = Encoding.ASCII.GetString(args.data);
            _botConnectionManager.Log("Controls List Received");
            _controlsInfoList = JsonUtility.FromJson<ControlsInfoList>(text);
            _waitForListUpdate = false;

            // TODO: get binary data (not worked now)
            // var memoryStream = new MemoryStream(args.data);
            // var bfd = new BinaryFormatter();
            // _botModel.ControlsInfoList = bfd.Deserialize(memoryStream) as ControlsInfoList;
        }

        private void OnSpyDataReceived(MessageEventArgs args)
        {
            if (args.data != null)
            {
                var json = Encoding.ASCII.GetString(args.data);
                Log(new Tuple<string, string>("Spy data received", json));
                // update spy data model
                var before = _botDataObjectWithEvents.GetEventsCount();
                JsonUtility.FromJsonOverwrite(json, _botDataObjectWithEvents);
                var after = _botDataObjectWithEvents.GetEventsCount();
                if (before != after)
                {   // Attention! Unity not overwrite list - it recreate it. DO NOT REMOVE THIS ATTENTION
                    Debug.LogError($"<color=red>Attention:</color> Events in list before JsonUtility.FromJsonOverwrite is {before} and after is {after}."
                                   + $"List was recreated and all event is lost. Please update BotDataObjectWithEvents List with correct values.");
                }
            }

            _waitForGameDataUpdate = false;
            // invoke OnSpyDataModelUpdated
        }
#endregion

        private async Task RequestControlsAsync(CancellationToken cancellationToken)
        {
            _waitForListUpdate = true;
            _botConnectionManager.SendToGame(BotConstants.RequestControlsList_BotToGame, null);

            while (_waitForListUpdate)
            {
                await Task.Yield();
                cancellationToken.ThrowIfCancellationRequested();
            }
        }

        private async Task RequestGameDataAsync(CancellationToken cancellationToken)
        {
            _waitForGameDataUpdate = true;
            _botConnectionManager.SendToGame(BotConstants.RequestSpyData_BotToGame, null);

            while (_waitForGameDataUpdate)
            {
                await Task.Yield();
                cancellationToken.ThrowIfCancellationRequested();
            }
        }

        private void ClickElement(ControlsInfo element)
        {
            var clickData = new ClickData()
            {
                Position = element.Rect.center
            };

            var textData = JsonUtility.ToJson(clickData, true);
            var byteData = Encoding.ASCII.GetBytes(textData);

            _botConnectionManager.SendToGame(BotConstants.SimulateClick_BotToGame, byteData);
        }

        public void StartBot()
        {
            if (_cancellationTokenSource == null)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                DoBot(_cancellationTokenSource.Token).FireAndForgetSafeAsync(Debug.LogError);
            }
        }

        public void StopBot()
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = null;
            }
        }

        private async Task DoBot(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await RequestControlsAsync(cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();

                await RequestGameDataAsync(cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();

                await Task.Delay(DELAY_AFTER_CONTROLS_RECEIVED, cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();

                var totalProbability = TotalProbability;
                if (totalProbability <= 0f)
                {
                    Log($"<color=red>Selected is null probability {totalProbability}</color>");
                    await Task.Delay(DELAY_ON_NO_CONTROL, cancellationToken);
                    continue;
                }

                var randomValue = UnityEngine.Random.Range(0, totalProbability);

                var selected = SelectedByProbability(GetActualProbabilities.ToList(), randomValue);
                if (selected == null)
                {
                    continue;
                }

                var excludeList = new List<BotControlProbability>();
                if (string.IsNullOrEmpty(selected.Path))
                {
                    excludeList = Probabilities.Where(x => string.Equals(x.Name, selected.Name) && !string.IsNullOrEmpty(x.Path)).ToList();
                }

                var shuffledList = new List<ControlsInfo>(ControlsInfoList.List);
                shuffledList.Shuffle();
                var element = shuffledList.FirstOrDefault(
                    x => selected.IsEqual(x.Name, x.FullPath) && !excludeList.Any(y => string.Equals(x.FullPath, y.Path)));
                if (element == null)
                {
                    Log($"<color=red>Element not found {selected.Name}  {selected.Path}</color>");
                    continue;
                }

                ClickElement(element);
                var message = string.IsNullOrEmpty(selected.ActionMessage)
                                  ? $"SELECTED BUTTON <color=green>{element.Name}</color> ( {element.FullPath} ) "
                                  : selected.ActionMessage;
                Log(message);
                selected.Event.Invoke();

                var delay = selected.DelayMsec == 0
                                ? DEFAULT_DELAY_AFTER_CLICK
                                : selected.DelayMsec;
                await Task.Delay(delay, cancellationToken);
            }
        }

        private BotControlProbability SelectedByProbability(List<BotControlProbability> list, float probabilityValue)
        {
            foreach (var value in list)
            {
                if (value.Probability > probabilityValue)
                {
                    return value;
                }

                probabilityValue -= value.Probability;
            }

            Log($"<color=red>Strange: not selected {probabilityValue}  List Count {list.Count()}</color>");
            return null;
        }
    }
}
