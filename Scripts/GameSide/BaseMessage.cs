﻿using System;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public abstract class BaseMessage : IBotToGameMessage
    {
        public abstract Guid Guid { get; }

        public event Action<Guid, byte[]> Send;
        public event Action<string> Log;

        public abstract void OnMessage(MessageEventArgs args);

        protected void SendInternal(Guid guid, byte[] data)
        {
            Send?.Invoke(guid, data);
        }

        protected void LogInternal(string message)
        {
            Log?.Invoke(GetType().Name + " in game message : " + message);
        }
    }
}