﻿using System;
using System.Text;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public class HelloMessage : BaseMessage
    {
        public override Guid Guid => BotConstants.HelloMessage_BotToGame;

        public override void OnMessage(MessageEventArgs args)
        {
            var text = Encoding.ASCII.GetString(args.data);
            LogInternal(text);
            SendInternal(BotConstants.HelloMessage_GameToBot, Encoding.ASCII.GetBytes("Hello from Player, I got message : " + text));
        }
    }
}