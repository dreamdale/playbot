﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public class RequestScreenshot : BaseMessage
    {
        public override Guid Guid => BotConstants.RequestScreenshot_BotToGame;

        public override void OnMessage(MessageEventArgs args)
        {
            LogInternal($"Screenshot requested");
            ScreenshotAsync().FireAndForgetSafeAsync(Debug.LogError);
        }

        private async Task ScreenshotAsync()
        {
            await Task.Yield();
            var screenshotTexture = ScreenCapture.CaptureScreenshotAsTexture();
            var screenshotBinaryData = screenshotTexture.EncodeToJPG();
//        var size  = GetMainGameViewSize();
//var size = Handles.GetMainGameViewSize();
            //      var data = FastScreenshot(Screen.width, Screen.height);
            // var display = Display.main;
            // var data = SlackAPISender.FastScreenshot(display.renderingWidth, display.renderingHeight);
            SendInternal(BotConstants.SendScreenshot_GameToBot, screenshotBinaryData);
        }

        // // C#
        // public static Vector2 GetMainGameViewSize()
        // {
        //     System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        //     System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        //     System.Object Res = GetSizeOfMainGameView.Invoke(null,null);
        //     return (Vector2)Res;
        // }
        //
        // // C#
        // public static EditorWindow GetMainGameView()
        // {
        //     System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        //     System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        //     System.Object Res = GetMainGameView.Invoke(null,null);
        //     return (EditorWindow)Res;
        // }


        public static byte[] FastScreenshot(int width, int height)
        {
            Texture2D tex = new Texture2D(width, height);
            tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex.Apply();
            return tex.EncodeToJPG();
        }
    }
}