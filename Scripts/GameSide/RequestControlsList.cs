﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public class RequestControlsList : BaseMessage
    {
        public override Guid Guid => BotConstants.RequestControlsList_BotToGame;

        public override void OnMessage(MessageEventArgs args)
        {
            ScanAndSendControlsListAsync().FireAndForgetSafeAsync(Debug.LogError);
        }

        private async Task ScanAndSendControlsListAsync() {
            var list = await ScanControlsListAsync();
            var res = JsonUtility.ToJson(list, true);
            SendInternal(BotConstants.SendControlsList_GameToBot, Encoding.ASCII.GetBytes(res));

            // TODO: send binary data (not worked now)
            // var memoryStream = new MemoryStream();
            // var bf = new BinaryFormatter();
            // bf.Serialize(memoryStream, list);
            // SendInternal(BotConstants.SendControls_GameToBit, memoryStream.ToArray());
        }

        private static Task<ControlsInfoList> ScanControlsListAsync(float xCount = 24f, float yCount = 32f)
        {
            var list = new ControlsInfoList();
            var width = Screen.width;
            var height = Screen.height;

            void SafeAddToList(Transform tr, Vector2 pos)
            {
                if (tr == null) return;

                var found = list.List.FirstOrDefault(x => x.Id == tr.GetInstanceID());
                if (found != null) return;

                list.List.Add(
                    new ControlsInfo()
                    {
                        Name = tr.name,
                        FullPath = tr.GetNameRecursive(),
                        Id = tr.GetInstanceID(),
                        Rect = new Rect(pos, new Vector2(2f, 2f))
                    });
            }

            var pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            var diffX = width / xCount;
            var diffY = height / yCount;
            for (var dy = diffY * 0.5f; dy < height; dy += diffY)
            {
                for (var dx = diffX * 0.5f; dx < width; dx += diffX)
                {
                    pointerData.position = new Vector3(dx, dy, 0);

                    var results = new List<RaycastResult>();
                    if (EventSystem.current == null)
                    {
                        return Task.FromResult(list);
                    }

                    EventSystem.current.RaycastAll(pointerData, results);

                    if (results.Count > 0)
                    {
                        var firstGO = results.FirstOrDefault().gameObject;
                        var newGameObject = ExecuteEvents.GetEventHandler<IPointerClickHandler>(firstGO);
                        if (newGameObject != null)
                        {
                            SafeAddToList(newGameObject.transform, new Vector2(pointerData.position.x, pointerData.position.y));
                            continue;
                        }

                        newGameObject = ExecuteEvents.GetEventHandler<IPointerDownHandler>(firstGO);
                        if (newGameObject != null)
                        {
                            SafeAddToList(newGameObject.transform, new Vector2(pointerData.position.x, pointerData.position.y));
                            continue;
                        }

                        if (newGameObject == null)
                        {
#if UNITY_EDITOR
                            Debug.LogWarning(firstGO + " handler is NULL");
#endif
                        }
                    }
                }

                Task.Yield();
            }

            return Task.FromResult(list);
        }
    }
}
