﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public class SimulateClick : BaseMessage
    {
        public override Guid Guid => BotConstants.SimulateClick_BotToGame;

        public override void OnMessage(MessageEventArgs args)
        {
            var text = Encoding.ASCII.GetString(args.data);
            var clickData = JsonUtility.FromJson<ClickData>(text);
            SimClick(clickData);
        }

        private static void SimClick(ClickData clickData)
        {
            var mouseData = new PointerInputModule.MouseButtonEventData();
            var pointerEvent = new PointerEventData(EventSystem.current);

            pointerEvent.position = clickData.Position;
            pointerEvent.eligibleForClick = true;
            pointerEvent.delta = Vector2.zero;
            pointerEvent.dragging = false;
            pointerEvent.useDragThreshold = true;
            pointerEvent.pressPosition = pointerEvent.position;

            var raycastResults = new List<RaycastResult>();
            if (EventSystem.current == null) return;
            EventSystem.current.RaycastAll(pointerEvent, raycastResults);
            pointerEvent.pointerCurrentRaycast = pointerEvent.pointerPressRaycast = raycastResults.FirstOrDefault();

            pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;
            mouseData.buttonData = pointerEvent;

            SimulateClickAsync(mouseData, clickData.TimeMSec).FireAndForgetSafeAsync(Debug.LogError);
        }

        private static async Task SimulateClickAsync(PointerInputModule.MouseButtonEventData mouseData, int pressTime)
        {
            var pressedGameObject = ExecuteEvents.ExecuteHierarchy(
                mouseData.buttonData.pointerPressRaycast.gameObject, mouseData.buttonData, ExecuteEvents.pointerDownHandler);

            await Task.Delay(pressTime);

            ExecuteEvents.Execute(pressedGameObject, mouseData.buttonData, ExecuteEvents.pointerUpHandler);

            ExecuteEvents.ExecuteHierarchy(
                mouseData.buttonData.pointerPressRaycast.gameObject, mouseData.buttonData, ExecuteEvents.pointerClickHandler);
        }
    }
}
