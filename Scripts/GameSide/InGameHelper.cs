﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Playbot;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

public static class InGameHelper
{
    private static IGameConnectionManager _gameConnectionManager;

    private static void Log(string message) => _gameConnectionManager?.Log($"Game : {message}");

    private static readonly Dictionary<Guid, IBotToGameMessage> _handlerInstances = new Dictionary<Guid, IBotToGameMessage>();

    public static Dictionary<Guid, UnityAction<MessageEventArgs>> AvailableActions()
    {
        return _handlerInstances.ToDictionary(item => item.Key, item => (UnityAction<MessageEventArgs>) item.Value.OnMessage);
    }

    public static void Register(Action<Guid, UnityAction<MessageEventArgs>> registerAction)
    {
        foreach (var pair in AvailableActions())
        {
            registerAction?.Invoke(pair.Key, pair.Value);
            Log(pair.Key + " In Game Helper Register " + pair.Value.Method.Name);
        }
    }

    public static void Unregister(Action<Guid, UnityAction<MessageEventArgs>> registerAction)
    {
        foreach (var pair in AvailableActions())
        {
            registerAction?.Invoke(pair.Key, pair.Value);
            Log(pair.Key + " In Game Helper Unregister " + pair.Value.Method.Name);
        }
    }

    static InGameHelper()
    {
        FillInheritedTypes();
    }

    public static void SetManager(IGameConnectionManager gameConnectionManager)
    {
        _gameConnectionManager = gameConnectionManager;
    }

    private static void OnSend(Guid guid, byte[] data)
    {
        _gameConnectionManager?.SendToBot(guid, data);
    }

    private static void OnLog(string message)
    {
        Log(message);
    }

    private static void FillInheritedTypes()
    {
        var type = typeof(IBotToGameMessage);
        var types =
            // AppDomain.CurrentDomain.GetAssemblies()
            // .SelectMany(s => s.GetTypes())
            Assembly.GetAssembly(type).GetTypes()
                    .Where(p => type.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);
        foreach (var typ in types)
        {
            var handlerInstance = (IBotToGameMessage) Activator.CreateInstance(typ);
            handlerInstance.Send += OnSend;
            handlerInstance.Log += OnLog;
            if (!_handlerInstances.ContainsKey(handlerInstance.Guid))
            {
                _handlerInstances.Add(handlerInstance.Guid, handlerInstance);
            }
            else
            {
                Debug.LogError($"{handlerInstance.Guid} always registered");
            }
        }
    }
}
