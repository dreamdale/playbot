﻿using System;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public interface IBotToGameMessage
    {
        Guid Guid { get; }
        event Action<Guid, byte[]> Send;
        event Action<string> Log;
        void OnMessage(MessageEventArgs args);
    }
}