﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.Scripting;

namespace Playbot
{
    [Preserve]
    public class RequestSpyData : BaseMessage
    {
        public override Guid Guid => BotConstants.RequestSpyData_BotToGame;

        public override void OnMessage(MessageEventArgs args)
        {
//            var text = Encoding.ASCII.GetString(args.data);
            // get spy data from game
//            var spyList = UnityEngine.Object.FindObjectsOfType<InGameSpy>(); // faster then GameObject.Find?
// TODO: Support many spies
            var go = GameObject.Find("Spy");
            if (go == null)
            {
                LogInternal("Not found Spy object");
                SendInternal(BotConstants.SendSpyData_GameToBot, null);
                return;
            }

            Action<List<Tuple<string, object>>> action = OnCustomDataReceived;
            go.SendMessage("GetDynamicData", action);
            LogInternal("Spy data requested from game Spy");
        }

        private void OnCustomDataReceived(List<Tuple<string, object>> data)
        {
            LogInternal("Spy data received from game Spy");
            var botGameDataListObject = new BaseBotDataObjectList();
            foreach (var entry in data)
            {
                botGameDataListObject.List.Add(
                    new BaseBotDataObject()
                    {
                        Name = entry.Item1,
                        Object = entry.Item2
                    });
            }

            // encode
            LogInternal("Spy data sent to Bot");
            var json = JsonUtility.ToJson(botGameDataListObject);
            var dataToSend = botGameDataListObject.List.Count > 0
                                 ? Encoding.ASCII.GetBytes(json)
                                 : null;
            LogInternal(json);


            SendInternal(BotConstants.SendSpyData_GameToBot, dataToSend);
        }
    }
}
