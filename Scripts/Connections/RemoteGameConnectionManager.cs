﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public class RemoteGameConnectionManager : IGameConnectionManager
    {
        public RemoteGameConnectionManager()
        {
            InGameHelper.SetManager(this);
            Register();
        }

        private void Register() => InGameHelper.Register(RegisterPlayerMessage);

        private void Unregister() => InGameHelper.Unregister(UnregisterPlayerMessage);
        public void RegisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args) => PlayerConnection.instance.Register(messageId, args);
        public void UnregisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args) => PlayerConnection.instance.Unregister(messageId, args);
        public void SendToBot(Guid messageId, byte[] data) => PlayerConnection.instance.Send(messageId, data);
        public void Log(string message) => Debug.LogError(message);
    }
}
