﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public class LocalConnectionManager : IGameConnectionManager, IBotConnectionManager
    {
        private static LocalConnectionManager _instance;
        public static IBotConnectionManager Instance => _instance ?? (_instance = new LocalConnectionManager());

        private readonly List<Tuple<Guid, UnityAction<MessageEventArgs>>> _actionsList = new List<Tuple<Guid, UnityAction<MessageEventArgs>>>();

        public LocalConnectionManager()
        {
            InGameHelper.SetManager(this);
            Register();
        }

        public void Register()
        {
            _actionsList.Clear();
            InGameHelper.Register(RegisterPlayerMessage);
        }

        private void Unregister()
        {
            InGameHelper.Unregister(UnregisterPlayerMessage);
        }

        public void RegisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args) =>
            _actionsList?.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(messageId, args));

        public void UnregisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args) =>
            _actionsList?.Remove(new Tuple<Guid, UnityAction<MessageEventArgs>>(messageId, args));

        public void RegisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args) =>
            _actionsList?.Add(new Tuple<Guid, UnityAction<MessageEventArgs>>(messageId, args));

        public void UnregisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args) =>
            _actionsList?.Remove(new Tuple<Guid, UnityAction<MessageEventArgs>>(messageId, args));

        public void SendToBot(Guid messageId, byte[] data) => SendTo(messageId, data);

        public void SendToGame(Guid messageId, byte[] data) => SendTo(messageId, data);

        private void SendTo(Guid messageId, byte[] data)
        {
            var result = _actionsList.FindAll(x => Equals(x.Item1, messageId));
            if (result.Count == 0)
            {
                Log($"Message {messageId} not registered");
                return;
            }

            foreach (var method in result.Select(pair => pair.Item2))
            {
                method.Invoke(new MessageEventArgs() { data = data });
            }
        }

        public void Log(string message) => Debug.LogWarning(message);
    }
}
