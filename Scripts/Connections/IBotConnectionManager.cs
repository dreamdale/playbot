﻿using System;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public interface IBotConnectionManager
    {
        void RegisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args);
        void UnregisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args);

        void SendToGame(Guid messageId, byte[] data);
        void Log(string message);
    }
}