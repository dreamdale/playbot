﻿using System;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public interface IGameConnectionManager
    {
        void RegisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args);
        void UnregisterPlayerMessage(Guid messageId, UnityAction<MessageEventArgs> args);

        void SendToBot(Guid messageId, byte[] data);
        void Log(string message);
    }
}