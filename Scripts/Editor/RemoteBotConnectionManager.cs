﻿using System;
using UnityEditor.Networking.PlayerConnection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public class RemoteBotConnectionManager : ScriptableObject, IBotConnectionManager
    {
        public void RegisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args) => EditorConnection.instance.Register(messageId, args);
        public void UnregisterEditorMessage(Guid messageId, UnityAction<MessageEventArgs> args) => EditorConnection.instance.Unregister(messageId, args.Invoke);
        public void SendToGame(Guid messageId, byte[] data) => EditorConnection.instance.Send(messageId, data);
        public void Log(string message) => Debug.LogError(message);
    }
}
