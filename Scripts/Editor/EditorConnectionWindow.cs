﻿using UnityEngine;
using UnityEditor;
using System.Text;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public class EditorConnectionWindow : EditorWindow
    {

        [MenuItem("Bot/Open Editor Connection Window")]
        private static void Init()
        {
            var window = (EditorConnectionWindow) GetWindow(typeof(EditorConnectionWindow));
            window.Show();
            window.titleContent = new GUIContent("Editor Connection Window");
        }

        private void OnEnable()
        {
            UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Initialize();
//            UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Register(BotConstants.HelloMessage_GameToBot, OnMessageEvent);
//        UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Register(BotConstants.SendScreenshot_MsgPlayerToEditor, OnScreenshot);
        }

        private void OnDisable()
        {
//            UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Unregister(BotConstants.HelloMessage_GameToBot, OnMessageEvent);
            //    UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Unregister(BotConstants.SendScreenshot_MsgPlayerToEditor, OnScreenshot);
            UnityEditor.Networking.PlayerConnection.EditorConnection.instance.DisconnectAll();
        }

        private void OnMessageEvent(MessageEventArgs args)
        {
            var text = Encoding.ASCII.GetString(args.data);
            Debug.Log("Message from player: " + text);
        }

        private void OnScreenshot(MessageEventArgs args)
        {
            System.IO.File.WriteAllBytes("Screenshot.jpg", args.data);
            Debug.Log("Screenshot Message from player: " + args.playerId);
        }

        private void OnGUI()
        {
            if (GUILayout.Button("REFRESH"))
            {
                UnityEditor.Networking.PlayerConnection.EditorConnection.instance.DisconnectAll();
                UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Initialize();
            }

            var playerCount = UnityEditor.Networking.PlayerConnection.EditorConnection.instance.ConnectedPlayers.Count;
            var builder = new StringBuilder();
            builder.AppendLine($"{playerCount} players connected.");
            var i = 0;
            foreach (var p in UnityEditor.Networking.PlayerConnection.EditorConnection.instance.ConnectedPlayers)
            {
                if (GUILayout.Button("Open remote connection to player " + p.name + "   Id: " + p.playerId))
                {
                    var remoteBotConnection = CreateInstance(typeof(RemoteBotConnectionManager));
                    EditorLocalConnection.Init(remoteBotConnection as RemoteBotConnectionManager);
                }

                if (GUILayout.Button("Send hello message to player " + p.name + " " + p.playerId))
                {
                    UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Send(
                        BotConstants.HelloMessage_BotToGame, Encoding.ASCII.GetBytes("Hello from Editor"), p.playerId);
                }

                builder.AppendLine(string.Format("[{0}] - {1} {2}", i++, p.name, p.playerId));
            }

            EditorGUILayout.HelpBox(builder.ToString(), MessageType.Info);

            if (GUILayout.Button("Send message to player"))
            {
                UnityEditor.Networking.PlayerConnection.EditorConnection.instance.Send(
                    BotConstants.HelloMessage_BotToGame, Encoding.ASCII.GetBytes("Hello from Editor"));
            }
        }
    }
}
