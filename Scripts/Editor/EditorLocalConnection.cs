using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.Threading;
using UnityEngine.Networking.PlayerConnection;

namespace Playbot
{
    public class EditorLocalConnection : EditorWindow
    {
        private IBotConnectionManager _botConnectionManager;
        private Vector2 _scrollPosition;
        private Vector2 _scrollInTabPosition;
        private string _textData = "Hello from Editor";

        private readonly List<object> _localLog = new List<object>();
        private int _selectedTab;

        private BotModel _botModel;

        private void Log(object obj)
        {
            _localLog.Add(obj);
            while (_localLog.Count > 15)
            {
                _localLog.RemoveAt(0);
            }

            Repaint();
        }

        [MenuItem("Bot/Open Editor Local Connection Window")]
        private static void Init()
        {
            Init(new LocalConnectionManager());
        }

        public static void Init(IBotConnectionManager botConnectionManager)
        {
            var window = (EditorLocalConnection) GetWindow(typeof(EditorLocalConnection));
            window.SetManager(botConnectionManager);
            window.Show();
            window.LoadData();
            window.Subscribe();
            window.titleContent = new GUIContent("Editor Local Connection Window");
        }

        public EditorLocalConnection()
        {
        }

        public void LoadData()
        {
            _botModel = BotModel.CreateBotModel();
            _botModel.FillList();
            _botModel.SetConnectionManager(_botConnectionManager);
            _botModel.Register();
            _botModel.Log += Log;
        }

        private void SetManager(IBotConnectionManager botConnectionManager)
        {
            _botConnectionManager = botConnectionManager;
        }

        public void Subscribe()
        {
            _botConnectionManager.RegisterEditorMessage(BotConstants.SendControlsList_GameToBot, OnControlsList);
        }

        private void OnControlsList(MessageEventArgs args)
        {
            var json = Encoding.ASCII.GetString(args.data);
            Log(new Tuple<string, string>("Controls List received", json));
        }

        private void OnGUI()
        {
            EditorGUILayout.HelpBox(" -= " + _botConnectionManager + " =- ", MessageType.Info);
            if (_botConnectionManager == null) return;

            _selectedTab = GUILayout.Toolbar(_selectedTab, new string[] { "Game Data", "Probabilities", "Input message" });
            switch (_selectedTab)
            {
                case 0:
                    ShowGameData();
                    break;
                case 1:
                    ShowProbabilities();
                    break;
                case 2:
                    _textData = GUILayout.TextArea(_textData, GUILayout.Height(120));
                    break;
            }

            if (ReferenceEquals(_botModel, null))
            {
                GUILayout.Label(" Bot in null ");
                return;
            }

            using (new GUILayout.HorizontalScope())
            {
                using (new GUILayout.VerticalScope(GUILayout.Width(340)))
                {
                    foreach (var pair in InGameHelper.AvailableActions())
                    {
                        if (GUILayout.Button("Send " + pair.Value.Target))
                        {
                            var byteData = Encoding.ASCII.GetBytes(_textData);
                            _botConnectionManager.SendToGame(pair.Key, byteData);
                        }
                    }

                    GUILayout.Space(10);
                    if (!_botModel.IsBotPlay)
                    {
                        if (GUILayout.Button("Play Bot"))
                        {
                            _botModel.StartBot();
                        }
                    }
                    else
                    {
                        if (GUILayout.Button("Stop Bot"))
                        {
                            _botModel.StopBot();
                        }
                    }
                }

                using (new GUILayout.VerticalScope())
                {
                    if (_botModel.ControlsInfoList != null)
                    {
                        foreach (var element in _botModel.ControlsInfoList.List)
                        {
                            var isMany = _botModel.ControlsInfoList.List.Count(x => string.Equals(x.Name, element.Name));
                            var nm = (isMany > 1)
                                         ? element.FullPath
                                         : element.Name;
                            nm = nm.Replace("Strange Root/UI Main Deapth 10 A/", "../");
                            if (GUILayout.Button(new GUIContent($"{nm}", $"Id: {element.Id:x}\nPath: {element.FullPath}"), GUILayout.MinWidth(420)))
                            {
                                var clickData = new ClickData()
                                {
                                    Position = element.Rect.center
                                };

                                _textData = JsonUtility.ToJson(clickData, true);
                                var byteData = Encoding.ASCII.GetBytes(_textData);

                                _botConnectionManager.SendToGame(BotConstants.SimulateClick_BotToGame, byteData);
                            }

                            var rect = GUILayoutUtility.GetLastRect();

                            var selectedProb = _botModel.Probabilities.FirstOrDefault(x => x.IsEqual(element.Name, element.FullPath));
                            if (selectedProb == null)
                            {
                                selectedProb = _botModel.Probabilities.FirstOrDefault(x => string.Equals(element.Name, x.Name));
                            }

                            if (selectedProb != null)
                            {
                                var totalProbability = _botModel.TotalProbability;
                                var probZ = ((totalProbability == 0f)
                                                 ? 1f
                                                 : totalProbability);
                                EditorGUI.ProgressBar(rect, selectedProb.Probability / probZ, $"{nm}");
                            }
                        }
                    }
                }
            }

            ShowBotHistory();
        }

        private void ShowGameData()
        {
            using (var scrollScope = new GUILayout.ScrollViewScope(_scrollInTabPosition, GUILayout.Height(250)))
            {
                _scrollInTabPosition = scrollScope.scrollPosition;
                foreach (var entry in _botModel.BotDataObjectWithEventList.List)
                {
                    GUILayout.Label(entry.Name + " = " + entry.ObjectData);
                }
            }
        }

        private void ShowProbabilities()
        {
            using (var scrollScope = new GUILayout.ScrollViewScope(_scrollInTabPosition, GUILayout.Height(250)))
            {
                _scrollInTabPosition = scrollScope.scrollPosition;
                var myLabel = new GUIStyle(EditorStyles.label)
                {
                    richText = true,
                    wordWrap = false,
                };
                foreach (var entry in _botModel.BotControlProbabilities.List)
                {
                    GUILayout.Label(entry.Name + "    Current = " + entry.Probability + "   Default = " + entry.DefaultProbability + "    " + ExtraData(entry.Probability, entry.DefaultProbability), myLabel);
                }
            }
        }

        private string ExtraData(float prob, float defProb)
        {
            if (prob < 0.1f && defProb > 0f)
            {
                return "<color=red>SUPRESSED</color>";
            }

            if (prob > defProb)
            {
                return "<color=green>INCREASED</color>";
            }

            return null;
        }

        private void ShowBotHistory()
        {
            using (var scrollScope = new GUILayout.ScrollViewScope(_scrollPosition))
            {
                _scrollPosition = scrollScope.scrollPosition;
                for (var i = 0; i < _localLog.Count; i++)
                {
                    var obj = _localLog[i];
                    switch (obj)
                    {
                        case Texture2D texture:
                            GUILayout.Label(texture, GUILayout.MaxHeight(500));
                            break;
                        case string str:
                            var myLabel = new GUIStyle(EditorStyles.label)
                            {
                                richText = true,
                                wordWrap = false,
                            };
                            GUILayout.Label(str, myLabel);
                            break;
                        case Action action:
                            action.Invoke();
                            break;
                        case Tuple<string, string> tuple:
                            if (GUILayout.Button(tuple.Item1, GUILayout.MaxWidth(340)))
                            {
                                _localLog.RemoveAt(i);
                                _localLog.Insert(i, tuple.Item2);
                                Debug.LogError(tuple.Item2);
                            }
                            break;
                    }
                }
            }
        }
    }
}
