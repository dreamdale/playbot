﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

public static class TaskUtilitiesCopy
{
    // copy from project to avoid reference to assembly
#pragma warning disable RECS0165 //Asynchronous methods should return a Task instead of void
    public static async void FireAndForgetSafeAsync(this Task task, Action<Exception> errorHandler = null)
#pragma warning restore RECS0165 //Asynchronous methods should return a Task instead of void
    {
        try
        {
            await task;
        }
        catch (OperationCanceledException)
        {
        }
        catch (Exception ex)
        {
            errorHandler?.Invoke(ex);
        }
    }

    public static string GetNameRecursive(this Transform tr)
    {
        var parent = tr.parent;
        return parent != null
                   ? GetNameRecursive(parent) + "/" + tr.name
                   : tr.name;
    }

    public static Rect GetScreenCoordinates(this RectTransform uiElement)
    {
        var worldCorners = new Vector3[4];
        uiElement.GetWorldCorners(worldCorners);
        var result = new Rect(
            worldCorners[0].x,
            worldCorners[0].y,
            worldCorners[2].x - worldCorners[0].x,
            worldCorners[2].y - worldCorners[0].y);
        return result;
    }

    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static object GetValue(this MemberInfo memberInfo, object forObject)
    {
        switch (memberInfo.MemberType)
        {
            case MemberTypes.Field:
                return ((FieldInfo) memberInfo).GetValue(forObject);
            case MemberTypes.Property:
                return ((PropertyInfo) memberInfo).GetValue(forObject);
            default:
                throw new NotImplementedException();
        }
    }
}
